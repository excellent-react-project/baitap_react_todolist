import ToDoList from "./BaiTapToDoList/ToDoList";
import ReactLifeCycle from "./ReactLifeCycle/ReactLifeCycle";

function App() {
  return (
    <div className="App">
    <ToDoList/>
    {/* <ReactLifeCycle/> */}
    </div>
  );
}

export default App;
