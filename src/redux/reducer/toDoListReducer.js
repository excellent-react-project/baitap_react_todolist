import { ToDoListDarkTheme } from "../../BaiTapToDoList/Theme/DarkTheme";
import { arrTheme } from "../../BaiTapToDoList/Theme/ThemeManager";
import {
  ADD_TASK,
  CHANGE_THEME,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from "../type/toDoListType";

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: 1, taskName: "task 1", done: true },
    { id: 2, taskName: "task 2", done: false },
    { id: 3, taskName: "task 3", done: true },
    { id: 4, taskName: "task 4", done: false },
  ],
  taskEdit: { id: -1, taskName: "", done: false },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TASK: {
      // Kiểm tra rỗng
      if (action.newTask.taskName.trim() === "") {
        alert("Task name is required !");
        return { ...state };
      }

      // Kiểm tra tồn tại
      let taskListUpdate = [...state.taskList];

      let index = taskListUpdate.findIndex(
        (task) => task.taskName === action.newTask.taskName
      );

      if (index !== -1) {
        alert("Task name already exists !");
        return { ...state };
      }

      taskListUpdate.push(action.newTask);

      // Xử lý xong thì gán taskList mới vào taskList hiện tại
      state.taskList = taskListUpdate;

      return { ...state };
    }
    case CHANGE_THEME: {
      // Tìm ra theme dựa vào action.themeId được chọn
      let theme = arrTheme.find((theme) => theme.id == action.themeId);
      if (theme) {
        // set lại theme cho redux
        state.themeToDoList = { ...theme.theme };
      }

      return { ...state };
    }
    case DONE_TASK: {

      let taskListUpdate = [...state.taskList];

      let index = taskListUpdate.findIndex((task) =>task.id === action.taskId);
   
      if (index !== -1) {
        taskListUpdate[index].done = true;
      }
      // state.taskList = taskListUpdate;
      return { ...state, taskList: taskListUpdate };
    }
    case DELETE_TASK: {
      let taskListUpdate = [...state.taskList];

      taskListUpdate = taskListUpdate.filter(task => task.id !== action.taskId);

      return { ...state, taskList: taskListUpdate};
    }
    case EDIT_TASK:{
      return {...state, taskEdit : action.task}
    }
    case UPDATE_TASK:{

      console.log(action.taskName);
      // Chỉnh sửa lai taskName của taskEdit
      state.taskEdit = {...state.taskEdit, taskName : action.taskName};

      // Tìm trong taskList cập nhật lại taskEdit người dùng update
      let taskListUpdate = [...state.taskList];

      let index = taskListUpdate.findIndex(task => task.id === state.taskEdit.id);

      if (index !== -1){
        taskListUpdate[index] = state.taskEdit;
      }

      state.taskList = taskListUpdate;


      state.taskEdit = {
        id: -1,
        taskName: '',
        done: false,
      }

      return {...state};
    }
    default:
      return { ...state };
  }
};
